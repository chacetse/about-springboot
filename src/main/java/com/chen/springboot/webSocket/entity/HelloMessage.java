package com.chen.springboot.webSocket.entity;

import lombok.Data;

/**
 * @Author xie
 * @Date 2018-11-18 16:11
 */
@Data
public class HelloMessage {
    private String name;
}
