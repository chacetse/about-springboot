package com.chen.springboot.webSocket.entity;

import lombok.Data;
import lombok.ToString;

/**
 * @Author xie
 * @Date 2018-11-18 16:11
 */
@Data
@ToString
public class Greeting {
    private String content;

    public Greeting(String content) {
        this.content = content;
    }
}
