package com.chen.springboot.webSocket.entity;

import lombok.Data;

/**
 * @Author xie
 * @Date 2018-11-18 21:18
 */
@Data
public class Message {
    private int id; //用户ID
    private String content;//发送内容
    private int pid;    //发送到用户
}
