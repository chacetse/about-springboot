package com.chen.springboot.webSocket.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

/**
 * @Author xie
 * @Date 2018-11-18 20:49
 */
@Component
public class STOMPConnectEventListener implements ApplicationListener<SessionConnectedEvent> {

    @Autowired
    SocketSessionMap socketSessionMap;

    @Override
    public void onApplicationEvent(SessionConnectedEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());

        // 服务端通过sha.getFirstNativeHeader("id")读取到客户端的ID，
        // 这个值需要网页客户端手动在header头部信息中设置。
        String userId = sha.getFirstNativeHeader("id");
        String sessionId = sha.getSessionId();

        // 判断客户端的连接状态
        switch (sha.getCommand()) {
            case CONNECT:
                System.out.println("上线");
                socketSessionMap.registerSession(userId, sessionId);
                break;
            case DISCONNECT:
                System.out.println("下线");
                socketSessionMap.removeSession(userId, sessionId);
                break;
            case SUBSCRIBE:
                System.out.println("订阅");
                break;
            case SEND:
                System.out.println("发送");
                break;
            case UNSUBSCRIBE:
                System.out.println("取消订阅");
                break;
            default:
                break;
        }

    }
}
